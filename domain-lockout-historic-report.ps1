<#
*  This script is provided as-is, no warrenty is provided or implied.
*  The author is NOT responsible for any damages or data loss that may occur
*  through the use of this script.  Always test, test, test before
*  rolling anything into a production environment.

#Some Ideas and Code from: http://noamwajnman.wordpress.com/2014/03/30/powershell-get-locked-out-ad-user-accounts-and-export-to-csv/
#Thanks Noam!

#If the script is hanging, or you see errors about RPC Server being unavialable, please see the troubleshooting section for get-winevent remoting here: http://www.computerperformance.co.uk/powershell/powershell_get_winevent_remote.htm
#>


#variables that control function expression
$sendEmail = $false
$outputCSV = $true

#Variables that control event log filter parameters
$howFarBackToLook_days = -7 #this must be negative in order to look back in time!
$eventlogHistoricDate = (get-date).addDays($howFarBackToLook_days)
$lockoutEventID = 4740 #event id of lockout

#Variables that control DC searches
$computerNames = @("hv-dc01") #names of your DC's seperated by commas since it's an array!

#Variables that control file output
$datestr = Get-Date -Format "yyyy-MM-dd"
$saveDirectory = "C:\temp\"
$fileName_uniquereport = "7_Day_Lockout_Report-" + ((get-date).AddDays($howFarBackToLook_days)).ToString('yyyy-MM-dd') + "_to_" + $datestr + ".csv"
$fileName_3timesreport = "3times_in_7_Days_Lockout_Report-" + ((get-date).AddDays($howFarBackToLook_days)).ToString('yyyy-MM-dd') + "_to_" + $datestr + ".csv"
$fullFilePath_uniquereport = $saveDirectory + $fileName_uniquereport
$fullFilePath_3timesreport = $saveDirectory + $fileName_3timesreport

#varible that controls email sending
$smtpServer = "localhost"
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$emailTemplateLocation_3times = "C:\AD_Lockout_Template_3times.html"
$emailTemplateLocation_unique = "C:\AD_Lockout_Template_unique.html"
$emailBody_3times = get-content $emailTemplateLocation_3times
$emailBody_unqiue = get-content $emailTemplateLocation_unique
$sendingEmailAddress = "william.udovich@cireson.com"


#Arrays that need to be initialized
$alllockouts = @()
$uniqueLockouts = @()
$groupedLockouts = @()
$allLockoutsMoreThan3Times = @()
$allGroupedAccountsLockedOutMoreThan3Times = @()


#Code to create a credential that you can then pass to Get-WinEvent if need be
#This way the script can be launched as anyone, and use an appropriate credential to call out if need be
<#
#username
$username = "<domain>\<username>"
#Convert password to secure string
$password = convertTo-SecureString -string "<password>" -asplaintext -force
#Create the pscredential object
$psCredential = new-object -typename system.management.automation.pscredential -argumentlist $username, $password
#>
 
function get-lockout-events
{
    Param( $computerNames )

    $allLockouts = @()
    $lockoutevents = @()

    foreach ($computerName in $computerNames)
    {
       #if using the psCredential above, swap these lines
       #$lockoutevents2 += Get-WinEvent -Credential $psCredential -ComputerName $computerName -FilterHashtable @{LogName='Security';Id=4740;StartTime=$eventlogHistoricDate}
       #need to error check here, it tosses an error if no events matching that criteria are found
       $lockoutevents += Get-WinEvent -ComputerName $computerName -FilterHashtable @{LogName='Security';Id=4740;StartTime=$eventlogHistoricDate}
    }

    foreach ($lockoutevent in $lockoutevents)
    {
        $lockoutUser = $lockoutevent.Properties[0].value
        $lockoutDevice = $lockoutevent.Properties[1].value
        $lockoutTime = $lockoutEvent.TimeCreated
        $lockoutSource = $lockoutEvent.ProviderName
        $lockoutMessage = $lockoutEvent.Message -creplace "`n|`r",""    
        $lockoutMessage = $lockoutMessage -creplace "`t"," "

        $lockout = "" | select "User","Email","Device","Time","Source","Message"
        $lockout.User = $lockoutUser
        $lockout.Email = (get-aduser $lockoutUser -properties EmailAddress).EmailAddress
        $lockout.Device = $lockoutDevice
        $lockout.Time = $lockoutTime
        $lockout.Source = $lockoutSource
        $lockout.Message = $lockoutMessage

        $allLockouts += $lockout

    }
    return $allLockouts
}

function get-info-from-grouped-lockouts
{
    Param ( $groupedLockoutArray )

    $allLockoutsMoreThan3Times = @()

    foreach ($account in $groupedLockoutArray)
    {
        $lockout = "" | select "User","Email","Device","Time","Source","Message"
        $lockout.User = $account.group[0].user
        $lockout.Email = (get-aduser $account.group[0].User -properties EmailAddress).EmailAddress
        $lockout.Device = $account.group[0].device
        $lockout.Time = $account.group[0].time
        $lockout.Source = $account.group[0].source
        $lockout.Message = $account.group[0].message

        $allLockoutsMoreThan3Times += $lockout
    }

    return $allLockoutsMoreThan3Times
}

function send-lockout-email
{
    param( $emailBody,
            $lockoutArray,
            $sendingEmailAddress )


    foreach ($lockout in  $lockoutArray)
    {
        $emailBody = $emailBody -replace "var_lockoutuser", $lockout.user
        $emailBody = $emailBody -replace "var_lockoutdevice", $lockout.device
        $emailBody = $emailBody -replace "var_lockouttime", $lockout.time
        $emailBody = $emailBody -replace "var_lockoutmessage", $lockout.message

        $eMail = new-object Net.Mail.MailMessage
        $eMail.From = $sendingEmailAddress
        $eMail.To.Add($lockout.Email)
        $eMail.subject = "AD Account Lockout Notification - " + $lockout.user
        $eMail.body = $emailBody
    
        $smtp.Send($eMail)
    }
}

function output-lockout-csv
{
    param( $lockoutArray,
            $csvPath )

    $lockoutArray | Export-Csv $csvPath -NoTypeInformation

}

##### MAIN #####

import-module activedirectory

$alllockouts = get-lockout-events -computerNames $computerNames
$uniqueLockouts = $alllockouts | sort -Property {$_.user} -Unique
$groupedLockouts = $alllockouts | group -Property User
$allGroupedAccountsLockedOutMoreThan3Times = $groupedLockouts | ?{$_.count -ge 3}

$allLockoutsMoreThan3Times = get-info-from-grouped-lockouts -groupedLockoutArray $allGroupedAccountsLockedOutMoreThan3Times

if ($outputCSV -eq $true)
{
    if ($allLockoutsMoreThan3Times.count -ge 1)
    {
        output-lockout-csv -lockoutArray $allLockoutsMoreThan3Times -csvPath $fullFilePath_3timesreport
    }
    if ($alllockouts.count -ge 1)
    {
        output-lockout-csv -lockoutArray $alllockouts -csvPath $fullFilePath_uniquereport
    }
}

if ($sendEmail -eq $true)
{
    if ($allLockoutsMoreThan3Times.count -ge 1)
    {
        send-lockout-email -emailBody $emailBody_3times -lockoutArray $allLockoutsMoreThan3Times -sendingEmailAddress $sendingEmailAddress
    }
    if ($uniqueLockouts.Count -ge 1)
    {
        send-lockout-email -emailBody $emailBody_unqiue -lockoutArray $uniqueLockouts -sendingEmailAddress $sendingEmailAddress
    }
}

remove-module activedirectory